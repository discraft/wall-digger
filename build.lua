local tArgs = { ... }
local function isLiquid()
    local _, data = turtle.inspectDown()
    local block = data.name
    if block == "minecraft:water" or block == "minecraft:lava" then
        turtle.select(4)
        if turtle.getItemCount() == 1 then
            turtle.select(3)
            turtle.placeUp()
            turtle.select(4)
            turtle.suckUp(63)
            turtle.select(3)
            turtle.digUp()
        end
        turtle.select(4)
        turtle.placeDown()
        return true
    else
        return false
    end
end

local function returnSupplies()
    if turtle.getItemCount(16) ~= 0 then
        turtle.select(2)
        turtle.placeUp()
        for i = 16,5,-1 do
            turtle.select(i)
            turtle.dropUp(64)
        end
        turtle.select(2)
        turtle.digUp()
    end
end

local function refuel()
    if turtle.getFuelLevel() < 32 then
        turtle.select(1)
        turtle.placeUp()
        turtle.suckUp(8)
        turtle.refuel()
        turtle.digUp()
    end
    return true
end

for i=1, tArgs[1] do
    for i=1, 511 do
        refuel()
        returnSupplies()
        isLiquid()
        if not turtle.forward() do
            turtle.dig()
            turtle.forward()
        end
    end
    turtle.turnLeft()
    turtle.turnLeft()
    turtle.digDown()
    turtle.down()
end
